#! /bin/bash

# fs graphql api

set -euo pipefail

SCRIPT=$(basename "$0")
ARGS=("$@")

main() {
  init
	
	if [[ ${#ARGS[@]} -lt 1 ]]; then
	  usage
		exit 1
	fi

	set_query 
	set_variables

	cmd=$(cat << 'EOF'
		curl -s -X POST -u "${username:-admin}:${password:-admin}" \
    	-H 'Content-Type: application/json' \
			-d '{"query": "'"${query}"'", "variables": '"${variables}"'}' \
			"${server}/${endpoint}" \
			-w '{"time_total": "%{time_total}"}'
EOF
)

eval echo "${cmd//\$\{password:-admin\}/<password>}"

eval "${cmd}"
}

init() {
	# shellcheck disable=SC2034
	server="${SERVER:-http://localhost:8080}" 
  # shellcheck disable=SC2034
	endpoint="${ENDPOINT:-graphql}"
	
	resources=("./query/"*".gql")
	resources_files=("${resources[@]##*/}")
	resources_names=("${resources_files[@]%%.gql}")
}

set_query() {
	resource="${ARGS[0]}"
  # shellcheck disable=SC2034
	query=$(sed -e 's/\"/\\\"/g' "./query/${resource}.gql" | tr '\n' ' ' | tr '\t' ' ') # escape quotes and remove tabs and new lines
}

set_variables() {
	variables='{}'

  for arg in "${ARGS[@]:1}"; do
		variable_name=${arg%%=*}
		variable_value=${arg#*=}
		if [[ ${variable_value} =~ ^#.+ ]]; then # send integer with prefiks #, ex. eierInstitusjonsnummer=#1234
			variable_value="${variable_value#*#}"
			flag='--argjson'  # needed for integer and other non String types
		else
			flag='--arg'
		fi
		variables=$(echo "$variables" | jq "${flag}" variable_value "${variable_value}"	'. + { '"${variable_name}"': $variable_value }')
	done
}

usage() {
	cat << EOF
usage: ${SCRIPT} <resource> <variable> ... <variable>

  * resource [ ${resources_names[@]} ]
EOF
}

main
