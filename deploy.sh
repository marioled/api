#!/bin/bash

set -xeuo pipefail

shellcheck "${0}"

script='api.sh'
env_folder='.api-env'

dev_folder="${HOME}/dev/fs-plattform-cli"
prod_folder="${HOME}/bin"
prod_script="${prod_folder}/${script%.*}"

shellcheck "${dev_folder}/${script}"

if [ -f "${prod_script}" ]; then chmod 700 "${prod_script}"; fi
cp "${dev_folder}/${script}" "${prod_script}"
sed -i 's/dev\/fs-plattform-cli/bin/' "${prod_script}"
chmod 550 "${prod_script}"

if [ -d "${prod_folder}/${env_folder}" ]; then
  rm -fr "${prod_folder}/${env_folder}.old"
  mv "${prod_folder}/${env_folder}" "${prod_folder}/${env_folder}.old"
  mkdir -p "${prod_folder}/${env_folder}"
fi
cp -r "${dev_folder}/${env_folder}" "${prod_folder}/"

cp "${dev_folder}/decode-id.sh" "${prod_folder}/decode-id"
cp "${dev_folder}/grafs.sh" "${prod_folder}/grafs"

echo -e "👍 api deployed"
