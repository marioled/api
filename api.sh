#!/bin/bash

script="${0##*/}"
env_folder=".api-env"
env_path="$HOME/dev/fs-plattform-cli/${env_folder}"
environment_files=( "${env_path}"/* )
environments=( "${environment_files[@]/${env_path}\//}" )

bold=$'\033[1m'
reset=$'\033[0m'
blink=$'\033[91;5m'

function error() {
    cat << EOF 

    ${blink}Error${reset} ${1}

    Read help "${script} -h"
EOF
    exit 1
}

function usage() {
    cat << EOF
${bold}Usage${reset}
    ${script} [options] resource [json_model] [key1=value1 key2=value2 ...]

${bold}Options${reset}
    -e   environment to use; see list of available environments
    -s   server; default http://localhost:4567
    -u   user or institution acronym / number 
    -d   dry run or debug; shows full curl command with hidden password
    -p   POST json model
    -h   help

${bold}Environments${reset}
    add environments in ${bold}${env_path}${reset} folder
    environment must define variable FS_PLATTFORM_PASSWORD, while FS_PLATTFORM_SERVER and FS_PLATTFORM_USER can be overrided 
    available environments: ${bold}${environments[@]}${reset}

${bold}Users${reset}
    available users: ${bold}uio, ntnu, uib${reset} ...; or use institution number ${bold}185, 194, 184${reset} ...

${bold}Resource${reset}
    resource or endpoint in form of /resource/:id/subresource/:sid

${bold}Query parameters${reset}
    parameters are given in form of key=value separated by space

${bold}Examples${reset}
    ${script} -e ${environments[0]} personer/1/eksternresultater
    ${script} personer/1/eksterneresultater kode=ABC           (default enivironmeent is local)
    ${script} -d -e ${environments[1]:-${environments[0]}} -u uio personer/1/eksterneresultater
    ${script} -e ${environments[2]:-${environments[0]}} -u 184 personer/1/eksterneresultater
    ${script} -dp -e ${environments[0]} personer/1/eksternresultater '{"field": "value", ... }'
    ${script} -p -e ${environments[0]} personer/1/eksternresultater ekstern_resultater.json
EOF
    exit 0
}

function show_payload() {
  if [[ -n ${1} ]]; then 
    echo '#'
    echo "# ${2}:" 
    jq . "${2}"
  fi
}

function username() {
  local name
  name="${1,,}"
  case "$name" in
	  184) user="${USER}_uib" ;;
	  185) user="${USER}_uio" ;;
	  194) user="${USER}_ntnu" ;;
    1615) user="${USER}_hk" ;;
	  1234|utv|fsutv) user="${USER}" ;;
	  *) user="${USER}_${name}" ;;
  esac
}

while getopts :e:u:s:pdh opt; do
  case "$opt" in
    e) environment="$OPTARG" ;;
    s) server="$OPTARG" ;;
    u) username "$OPTARG" ;;
    p) post="true" ;;
    d) dry_run="true" ;;
    h) usage ;;
    \?)       commands="$commands $OPTARG" ;;
  esac
done

shift $((OPTIND - 1))

[[ ${#} -eq 0 ]] && error "Script needs at least one parameter ressource (endpoint)"

environment="${environment:-${FS_PLATTFORM_ENV:-local}}"

if [ -f "${env_path}/${environment}" ]; then
  # shellcheck source=/dev/null
  source "${env_path}/${environment}"
else
  error "Environment option ${bold}${environment}${reset} is not defined in ${env_path} folder!"
fi

server="${server:-${FS_PLATTFORM_SERVER:-http://localhost:8080}}"
user="${user:-${FS_PLATTFORM_USER:-${USER}}}"
password="${FS_PLATTFORM_PASSWORD}"

: "${server?}" "${user?}" "${password?}" # checks all parameters are defined

resource="${1}"
shift

if [[ ${post} == "true" ]]; then
  if [[ -f "${1}" ]]; then
    file_prefix="@"
  fi
    payload="${1}"
  shift
else
  for (( i=1; i <= "$#"; i++ )); do
    [[ ! "${!i}" = *"="* ]] && error "Missing '=' in query paramter ${bold}${!i}${reset}"
    if [[ i -eq 1 ]]; then 
      query_params="?${!i}"
    else
      query_params="${query_params}&${!i}"
    fi
  done
fi

if [[ "${post}" == "true" ]]; then
  if [[ "${dry_run}" == "true" ]]; then
    echo '#'
    echo "# curl -X POST -s -u ${user}:******* --data ${file_prefix}${payload}  \\"
    echo "#      -H 'Content-Type: application/json' -H 'Accept: application/json' \\"
    echo "#      -L '${server}/${resource}'"
    show_payload "${file_prefix}" "${payload}"
  else
    curl -X POST -s -u "${user}:${password}" --data "${file_prefix}${payload}"  \
         -H 'Content-Type: application/json' -H 'Accept: application/json' \
         -L "${server}/${resource}"
    echo
  fi
else
  if [[ "$dry_run" == "true" ]]; then
    echo '#'
    echo "# curl -X GET -s -u ${user}:******* -H 'Accept: application/json' \\"
    echo "#      -L '${server}/${resource}${query_params}' \\"
    echo "# | jq ."
  else
    curl -X GET -s -u "${user}:${password}" -H "Accept: application/json" \
         -L "${server}/${resource}${query_params}" \
    | jq .
    echo
  fi
fi
