#! /bin/bash

# Decode Id
#
# input: fs-plattform id (base64 encoded string)
# output: info about id and components in json format

set -euo pipefail

main() {
	id="$1"
	
	repo='git@gitlab.sikt.no:fs/fs-plattform.git'
	repo_head='main'
	views_file='./kjerneapi-db/src/main/resources/configuration.xml'
	
	decoded_id=$(decode_id "${id}")
	view_id="${decoded_id%:*}"
  id_component_values="${view_id},${decoded_id#*:}"

	git archive --remote="${repo}" "$repo_head" "$views_file" | tar -x

	view=$(cat "$views_file" | xq '.configuration.views.view[] | select (.viewId == "'"${view_id}"'")')

	id_component_names="view_id,$(echo -n "$view" \
		| jq -r '.ids.id | if type == "array" then .[0].columns.column else .columns.column end | join(",") | ascii_downcase'
	)"

	id_components=$(echo -n "${id_component_values}" \
			| column --table-name "id_components" -s ',' --json --table-columns "${id_component_names}" \
			| jq '.id_components[0]'
		)

	echo -n "$view" \
		| jq '. | {
		  "id": "'"${id}"'",
			"decoded_id": "'"${decoded_id}"'",
			"view_id": .viewId,
			"view_name": .viewName | ascii_downcase
		}' | jq --argjson id_components "${id_components}" '. + {id_components: $id_components}'
}

decode_id() {
	local encoded_id
	encoded_id="${1}"

	encoded_id_length=${#encoded_id}

	case "$(( encoded_id_length % 4 ))" in
		1) encoded_id="${encoded_id}===" ;;
		2) encoded_id="${encoded_id}==" ;;
		3) encoded_id="${encoded_id}=" ;;
		*) ;;
	esac

  echo -n "$encoded_id" | base64 -d
}

if [ -p /dev/stdin ]; then
	read -r id
elif [[ $# -eq 1 ]]; then
	id="$1"
else
	echo "usage: $(basename "$0") <id>"
	echo "       echo -n <id> | decode-id"
	exit 1
fi

main "${id}"
